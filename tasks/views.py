from django.views.generic import CreateView, ListView, UpdateView
from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

# Create your views here.


class create_tasks(LoginRequiredMixin, CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "start_date", "due_date", "project", "assignee"]

    def get_queryset(self):
        return Task.objects.filter(is_completed=self.request.user)

    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


class list_tasks(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class update_tasks(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["is_completed"]

    def get_success_url(self):
        return reverse_lazy("show_my_tasks")
