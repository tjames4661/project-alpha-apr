from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView
from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin


# Create your views here.


class list_projects(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class detail_projects(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "projects/detail.html"


class create_projects(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]

    def get_success_url(self):
        return reverse("show_project", args=[self.object.id])
