from django.urls import path
from .views import create_tasks, list_tasks, update_tasks


urlpatterns = [
    path("create/", create_tasks.as_view(), name="create_task"),
    path("mine/", list_tasks.as_view(), name="show_my_tasks"),
    path("<int:pk>/complete/", update_tasks.as_view(), name="complete_task"),
]
