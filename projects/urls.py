from django.urls import path
from .views import list_projects, detail_projects, create_projects

urlpatterns = [
    path("", list_projects.as_view(), name="list_projects"),
    path("<int:pk>/", detail_projects.as_view(), name="show_project"),
    path("create/", create_projects.as_view(), name="create_project"),
]
