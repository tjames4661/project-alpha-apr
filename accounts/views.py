from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model, authenticate
from tracker.settings import LOGIN_REDIRECT_URL
from django.contrib.auth.models import User

# Create your views here.

user = get_user_model()


def signup(request):
    print(request.method)
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password1"]
            user = User.objects.create_user(
                username, email=None, password=password
            )
            authenticate(request, username=username, password=password)
            user.save()
            return redirect(LOGIN_REDIRECT_URL)
    else:
        form = UserCreationForm()
    return render(
        request,
        template_name="registration/signup.html",
        context={"form": form},
    )
